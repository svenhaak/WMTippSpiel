﻿# DHBW Mannheim - WMTippSpiel
Dies ist das Tippspiel für die WM 2018, des Kurses TINF16ITNS.
Das Projekt wurde im Rahmen der Software Engineering Vorlesung erstellt.

Git Upstream Repository: https://gitlab.com/svenhaak/WMTippSpiel.git
Aktivitätsverlauf des Coder Teams: https://gitlab.com/svenhaak/WMTippSpiel/activity