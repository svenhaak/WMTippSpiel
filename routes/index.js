﻿'use strict';
const express = require('express');
const router = express.Router();
const models = require('../models');
const restrictions = require('../restrictions');
const passwordHash = require('password-hash');

/* GET home page. */
router.get('/', function (req, res) {
	if (!req.session.user)
		res.render('login', { title: 'Willkommen!', user: req.session.user });
	else {
		const top3Promise = models.User.findAll({
			order: [['points', 'DESC'], ['prename', 'ASC']],
			where: {
				isAdministrator: false,
				points: {
					[models.sequelize.Op.gte]: models.sequelize.literal('(SELECT DISTINCT `points` FROM `Users` ORDER BY `points` DESC LIMIT 1 OFFSET 2)')
				}
			}
		});
		const upcoming_matchesPromise = models.Match.findAll({
			where: {
				start_time: {
					[models.sequelize.Op.gt]: models.sequelize.fn('NOW'),
				},
				id: {
					[models.sequelize.Op.notIn]: models.sequelize.literal('(SELECT matchID FROM Bets WHERE userID = ' + req.session.user.id + ')')
				}
			},
			include: [
				{model: models.Team, as: 'TeamA', required: true},
				{model: models.Team, as: 'TeamB', required: true}
			],
			order: ['start_time'],
			limit: 3
		});
		Promise.all([top3Promise, upcoming_matchesPromise]).then(([top3, upcoming_matches]) => 
			res.render('', { title: 'Home', user: req.session.user, top3, upcoming_matches })
		);
	}
});

router.post('/login', function (req, res) {
	models.User.findOne({ where: { login: req.body.login } }).then(user => {
		if (user && passwordHash.verify(req.body.password, user.password)) {
			req.session.user = user;
		}
		else {
			res.flash('Fehler!', 'Dein Benutzername / Passwort ist ungültig!');
		}
		res.redirect('/');
	});
});

router.get('/logout', restrictions.onlyAuthenticated, function (req, res) {
	delete req.session.user;
	res.redirect('/');
});

router.get('/profile', restrictions.onlyAuthenticated, function (req, res) {
	res.render('profile', { title: 'Benutzerprofil', user: req.session.user });
});

router.post('/profile', restrictions.onlyAuthenticated, function (req, res) {
	const namerestriction = /^[A-Z]{1}[a-z]{2,15}$/;
	if (!req.body.prename.match(namerestriction) || !req.body.lastname.match(namerestriction)) {
		res.flash('Fehler!', 'Dein Vor-/Nachname entspricht nicht den Kriterien!');
		res.redirect('/profile');
		return;
	}
	if (!req.body.login.match(/^[A-Za-z0-9._]{4,20}$/)) {
		res.flash('Fehler!', 'Dein Login muss 4-20 Zeichen lang sein (erlaubt: A-Z, a-z, 0-9, `.` und `_`)');
		res.redirect('/profile');
		return;
	}
	models.User.update({
		prename: req.body.prename,
		lastname: req.body.lastname,
		login: req.body.login
	}, {
		where: {
			id: req.session.user.id
		}
	}).then(() => {
		req.session.user.prename = req.body.prename;
		req.session.user.lastname = req.body.lastname;
		req.session.user.login = req.body.login;
		res.redirect('/profile');
	});
});

router.post('/passwort_aendern', restrictions.onlyAuthenticated, function (req, res){
	if(!passwordHash.verify(req.body.old_password, req.session.user.password)) {
		res.flash('Fehler!', 'Altes Passwort falsch!');
		res.redirect('/profile');
		return;
	}
	if(req.body.new_password !== req.body.new_password_confirmation){
		res.flash('Fehler!', 'Passwort-Bestätigung falsch!');
		res.redirect('/profile');
		return;
	}
	if (req.body.new_password.length < 4) {
		res.flash('Fehler!', 'Das Passwort muss min. 4 Zeichen lang sein!');
		res.redirect('/profile');
		return;
	}
	models.User.update({
		password: passwordHash.generate(req.body.new_password)},
		{ where: { id: req.session.user.id } }
	).then(count => {
		res.flash('Erfolg!', 'Passwort wurde geändert.');
		res.redirect('/profile');
	});
});


module.exports = router;