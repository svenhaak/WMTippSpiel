'use strict';
const express = require('express');
const router = express.Router();
const models = require('../models');
const restrictions = require('../restrictions');

router.get('/wetten', restrictions.onlyPlayer, function (req, res){
	const groupStagePromise = models.Group.findAll().then(groups => {
		return models.Gameday.findAll({
			where: {
				isPreliminary: true
			},
			include: [{
				model: models.Match,
				include: [
					'TeamA',
					'TeamB',
					{ model: models.Bet, where: { userID: req.session.user.id }, required: false }
				]
			}]
		}).then(gamedays => {
			for (const group of groups) {
				group.Gamedays = [];
				for (const gameday of gamedays) {
					let new_gameday = JSON.parse(JSON.stringify(gameday)); // In dieser behinderten Sprache
												// ist das ansscheinend die einfachste möglichkeit für ein deep clone
					 new_gameday.Matches = new_gameday.Matches.filter(match => {
						return match.teamAID == group.teamAID || match.teamBID == group.teamAID
							|| match.teamAID == group.teamBID || match.teamBID == group.teamBID
							|| match.teamAID == group.teamCID || match.teamBID == group.teamCID
							|| match.teamAID == group.teamDID || match.teamBID == group.teamDID;
					});
					group.Gamedays.push(new_gameday);
				}
			}
			return groups;
		});
	});
	const finalsPromise = models.Gameday.findAll({
		where: {
			isPreliminary: false
		},
		include: [{ model: models.Match, include: [
			{model: models.Team, as: 'TeamA', required: true},
			{model: models.Team, as: 'TeamB', required: true},
			{ model: models.Bet, where: { userID: req.session.user.id }, required: false } ] }
		]
	});
	Promise.all([groupStagePromise, finalsPromise]).then(([groups, finals]) => {
		const now =  new Date();
		res.render('player/bets', { title: 'Wetten', user: req.session.user, groups, finals, now });
	});
});

router.post('/wetten', restrictions.onlyPlayer, function (req, res) {
	//TODO testen ob spiel in Zukunft
	models.Bet.upsert({
		matchID: req.body.matchID,
		userID: req.session.user.id,
		teamAGoals: req.body.teamAGoals,
		teamBGoals: req.body.teamBGoals
	});
	res.send();
});

router.get('/spielplan', restrictions.onlyPlayer, function (req, res) {
	const groupsPromise = 	models.Group.findAll({
		include: ['TeamA', 'TeamB', 'TeamC', 'TeamD']
	}).then(groups => {
		return models.Gameday.findAll({
			where: {
				isPreliminary: true
			},
			include: [{
				model: models.Match,
				include: ['TeamA', 'TeamB']
			}]
		}).then(gamedays => {
			for (let group of groups) {
				group.Gamedays = [];
				for (const gameday of gamedays) {
					const new_gameday = JSON.parse(JSON.stringify(gameday)); // In dieser behinderten Sprache
												// ist das ansscheinend die einfachste möglichkeit für ein deep clone
					 new_gameday.Matches = new_gameday.Matches.filter(match => {
						return match.teamAID == group.teamAID || match.teamBID == group.teamAID
							|| match.teamAID == group.teamBID || match.teamBID == group.teamBID
							|| match.teamAID == group.teamCID || match.teamBID == group.teamCID
							|| match.teamAID == group.teamDID || match.teamBID == group.teamDID;
					});
					group.Gamedays.push(new_gameday);
				}	
			}
			return groups;
		});
	});

	const finalsPromise = models.Gameday.findAll({
		where: {
			isPreliminary: false,
		},
		include: [{
			model: models.Match,
			include:[
				{ model: models.Team, as:'TeamA', required: true },
				{ model: models.Team, as:'TeamB', required: true },
			]
		}]
	});
	
	Promise.all([groupsPromise, finalsPromise]).then(([groups, finals]) => {
		for (const group of groups) {
			group.Teams = [group.TeamA, group.TeamB, group.TeamC, group.TeamD];
			group.Teams.sort((a, b) => a.preliminaryPoints !== b.preliminaryPoints ?
										a.preliminaryPoints < b.preliminaryPoints :
										(a.goalDiff !== b.goalDiff ?
											a.goalDiff < b.goalDiff :
											a.goalsShot < b.goalsShot));
		}
		res.render('player/schedule', { title: 'Spielplan', user: req.session.user, groups, finals });
	});
});

router.get('/rangfolge', restrictions.onlyPlayer, function(req, res) {
	models.Usertipgroup.findAll({where: {
			userID: req.session.user.id
		},
		include: [{model: models.Tipgroup, include: [models.User]}],
		order: models.sequelize.literal('`Tipgroup->Users`.`points` DESC, `Tipgroup->Users`.`prename` ASC')
	}).then(usertipgroups => {
		res.render('player/ranking', { title: 'Tippgruppen Rang', user: req.session.user, usertipgroups })
	});
});

router.get('/rangfolge/global', restrictions.onlyPlayer, function(req, res) {
	models.User.findAll({where: {isAdministrator: 0}, order: [['points', 'DESC'], ['prename', 'ASC']]}).then(players => {
		res.render('player/ranking/global', {title: 'Rangfolge Global', user: req.session.user, players});
	});    
});

router.get('/rangfolge/vergleich', restrictions.onlyPlayer, function (req, res) {
	res.render('player/ranking/comparison', {title: 'Vergleich', user: req.session.user});
});

router.get('/rangfolge/vergleich/:name', function (req, res) {
	models.User.findAll({
		where: {
			isAdministrator: false,
			login: {
				[models.sequelize.Op.like]: `%${req.params.name}%`
			}
		}
	}).then(players => res.send({user: req.session.user, players}));
});

module.exports = router;