'use strict';
const express = require('express');
const router = express.Router();
const models = require('../models');
const sequelize = require('sequelize');
const passwordHash = require('password-hash');
const restrictions = require('../restrictions');

router.get('/ergebnisse_eintragen', restrictions.onlyAdminstrator, function (req, res) {
	const groupStagePromise = models.Group.findAll().then(groups => {
		return models.Gameday.findAll({
			where: {
				isPreliminary: true
			},
			include: [{
				model: models.Match,
				include: [
					'TeamA',
					'TeamB',
					{ model: models.Bet, where: { userID: req.session.user.id }, required: false }
				],
				where: {
					start_time: {
						[models.sequelize.Op.lt]: (new Date()).toISOString()
					}
				}
			}]
		}).then(gamedays => {
			for (const group of groups) {
				group.Gamedays = [];
				for (const gameday of gamedays) {
					let new_gameday = JSON.parse(JSON.stringify(gameday)); // In dieser behinderten Sprache
												// ist das ansscheinend die einfachste möglichkeit für ein deep clone
					 new_gameday.Matches = new_gameday.Matches.filter(match => {
						return match.teamAID == group.teamAID || match.teamBID == group.teamAID
							|| match.teamAID == group.teamBID || match.teamBID == group.teamBID
							|| match.teamAID == group.teamCID || match.teamBID == group.teamCID
							|| match.teamAID == group.teamDID || match.teamBID == group.teamDID;
					});
					group.Gamedays.push(new_gameday);
				}
			}
			return groups;
		});
	});
	const finalsPromise = models.Gameday.findAll({
		where: {
			isPreliminary: false
		},
		include: [{
			model: models.Match,
			include: [
				{model: models.Team, as: 'TeamA', required: true},
				{model: models.Team, as: 'TeamB', required: true},
				{ model: models.Bet, where: { userID: req.session.user.id }, required: false }
			],
			where: {
				start_time: {
					[models.sequelize.Op.lt]: (new Date()).toISOString()
				}
			}
		}]
	});
	Promise.all([groupStagePromise, finalsPromise]).then(([groups, finals]) => {
		const now =  new Date();
		res.render('administration/enter_results', { title: 'Ergebniss eintragen', user: req.session.user, groups, finals });
	});

});

router.post('/ergebnisse_eintragen', restrictions.onlyAdminstrator, function (req, res) {
	models.Match.findById(req.body.matchID, {include: ['TeamA', 'TeamB', models.Bet]}).then(match => {
		//Spielstand speichern
		match.teamAGoals = req.body.teamAGoals;
		match.teamBGoals = req.body.teamBGoals;
		match.save().then(() => {
			const teamsOfMatch = [match.TeamA, match.TeamB];
			for(const team of teamsOfMatch) {
				// Anzahl gewonnener Spiele eines Teams
				models.Match.findAll({
					include:[{model: models.Gameday, where: {isPreliminary: true}, attributes: []}],
					where: {
						[models.sequelize.Op.or]: [
							{ teamAID: team.id },
							{ teamBID: team.id }
						],
						start_time: {
							[models.sequelize.Op.lt]: new Date()
						}
					}
				}).then(matchesOfTeam => {
					const inTeamA = matchesOfTeam.filter(match => match.teamAID == team.id);
					const teamAwins = inTeamA.filter(match => match.teamAGoals > match.teamBGoals);
					const teamAdraws = inTeamA.filter(match => match.teamAGoals == match.teamBGoals);
					const inTeamB = matchesOfTeam.filter(match => match.teamBID == team.id);
					const teamBwins = inTeamB.filter(match => match.teamBGoals > match.teamAGoals);
					const teamBdraws = inTeamB.filter(match => match.teamBGoals == match.teamAGoals);
					const wins = teamAwins.concat(teamBwins);
					const draws = teamAdraws.concat(teamBdraws);
					team.preliminaryPoints = wins.length * 3 + draws.length * 1;
					team.goalsShot = inTeamA.reduce((prev, match) => prev + match.teamAGoals, 0) 
									+ inTeamB.reduce((prev, match) => prev + match.teamBGoals, 0);
					team.goalsAgainst = inTeamA.reduce((prev, match) => prev + match.teamBGoals, 0) 
										+ inTeamB.reduce((prev, match) => prev + match.teamAGoals, 0);
					team.goalDiff = team.goalsShot - team.goalsAgainst;
					team.save();
				});
			}
			const betPromises = [];
			const matchGoalDiff = match.teamAGoals - match.teamBGoals;
			const winner = matchGoalDiff < 0 ? -1 : (matchGoalDiff > 0 ? 1 : 0);
			for (const bet of match.Bets) {
				const betGoalDiff = bet.teamAGoals - bet.teamBGoals;
				const betWinner = betGoalDiff < 0 ? -1 : (betGoalDiff > 0 ? 1 : 0);

				if (bet.teamAGoals == match.teamAGoals && bet.teamBGoals == match.teamBGoals)
					bet.points = 5;
				else if (betGoalDiff == matchGoalDiff)
					bet.points = 3;
				else if (betWinner == winner)
					bet.points = 2;
				else
					bet.points = 0;
				betPromises.push(bet.save());
			}
			Promise.all(betPromises).then(() => {
				models.sequelize.query(`UPDATE Users JOIN (SELECT userID as id, SUM(Bets.points) as points FROM Bets GROUP BY userID) p	SET Users.points = p.points	WHERE Users.id = p.id AND Users.id IN (${match.Bets.map(bet_ => bet_.userID).toString()});`, 
					{type: models.sequelize.QueryTypes.UPDATE}).then(() => 	res.send());
			});
		});
	});
});

router.get('/tippgruppen', restrictions.onlyAdminstrator, function (req, res) {
	models.Tipgroup.findAll().then(tipgroups => {
		res.render('administration/tipgroup', { title: 'Tippgruppen', user: req.session.user, tipgroups });
	});
});

router.post('/tippgruppen/erstellen', restrictions.onlyAdminstrator, function (req, res) {
	models.Tipgroup.create({name: req.body.name});
	res.redirect('/tippgruppen');
});

router.post('/tippgruppen/entfernen', restrictions.onlyAdminstrator, function (req, res) {
	models.Usertipgroup.destroy({where: {tipgroupID: req.body.tipgroupID}});
	models.Tipgroup.destroy({ where: {
		id: req.body.tipgroupID
	}});
	res.send();
});

router.post('/tippgruppen/spieler', restrictions.onlyAdminstrator, function (req, res) {
	models.User.findAll({
		where: {
			isAdministrator: false
		},
		include: [{
			model: models.Usertipgroup,
			where: {
				tipgroupID: req.body.tipgroup_id
			},
			required: false}]
	}).then(player => {
		res.send(player);
	});
});

router.post('/tippgruppen/spieler/aendern', restrictions.onlyAdminstrator, function (req, res) {
	models.Usertipgroup.destroy({where: {
		tipgroupID: req.body.tipgroup_id
	}});
	const new_records = [];
	for (let player_id in req.body) {
		player_id = Number.parseInt(player_id);
		if(isNaN(player_id) === false)
			new_records.push({userID: player_id, tipgroupID: req.body.tipgroup_id});
	}
	models.Usertipgroup.bulkCreate(new_records);
	res.redirect('/tippgruppen');
});

router.get('/benutzer', restrictions.onlyAdminstrator, function (req, res) {
	models.User.findAll().then(users => {
		res.render('administration/user', { title: 'Nutzer', user: req.session.user, users });
	});
});

router.post('/benutzer/erstellen', restrictions.onlyAdminstrator, function (req, res) {
	models.User.create({
		prename: req.body.prename,
		lastname: req.body.lastname,
		login: req.body.login,
		isAdministrator: false,
		password: passwordHash.generate(req.body.password)
	});
	res.redirect('/benutzer');
});

router.post('/benutzer/entfernen', restrictions.onlyAdminstrator, function (req, res) {
	models.Usertipgroup.destroy({
		where: {
			userID: req.body.userID
		}
	}).then(() => {
		models.User.destroy({
			where: {
				id : req.body.userID
			}
		}).then(() => res.send());
	});
});

router.get('/ko_auswahl', restrictions.onlyAdminstrator, function (req, res) {
	const gdPromise = models.Gameday.findAll({
		where: {
			isPreliminary: false
		},
		include: [{model: models.Match, include: ['TeamA', 'TeamB']}]
	});
	const teamsPromise = models.Team.findAll();
	Promise.all([gdPromise, teamsPromise]).then(([gamedays, teams]) => {
		res.render('administration/select_finals',{ title: 'KO Auswahl', user: req.session.user, gamedays, teams });
	});
});

router.post('/ko_auswahl', restrictions.onlyAdminstrator, function (req, res) {
	models.Match.update({
		teamAID: req.body.teamAID,
		teamBID: req.body.teamBID
	}, {
		where: {
			id: req.body.matchID
		}
	}).then(() => res.send());
});

module.exports = router;