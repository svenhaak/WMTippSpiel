﻿module.exports.onlyAuthenticated = function (req, res, next) {
	if (!req.session.user)
		next(new Error('Melde dich an um Zugriff auf diesen Bereich zu erhalten!'));
	next();
}

module.exports.onlyPlayer = function(req, res, next) {
	if (!req.session.user || req.session.user.isAdministrator)
		next(new Error('Nur angemeldete "Spieler" dürfen diese Funktion verwenden!'));
	next();
}

module.exports.onlyAdminstrator = function (req, res, next) {
	if (!req.session.user || !req.session.user.isAdministrator)
		next(new Error('Nur angemeldete "Administatoren" dürfen diese Funktion verwenden!'));
	next();
}