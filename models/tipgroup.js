'use strict';
module.exports = (sequelize, DataTypes) => {
	const Tipgroup = sequelize.define('Tipgroup', {
		name: DataTypes.STRING
	});

	Tipgroup.associate = function (models) {
		models.Tipgroup.belongsToMany(models.User, {
			through: {
				model: models.Usertipgroup,
				unique: false,
		  },
		  foreignKey: 'tipgroupID'
		});
	};

	return Tipgroup;
};