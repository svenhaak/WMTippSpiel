'use strict';
module.exports = (sequelize, DataTypes) => {
	const Match = sequelize.define('Match', {
		teamAID: DataTypes.INTEGER,
		teamAGoals: DataTypes.INTEGER,
		teamBID: DataTypes.INTEGER,
		teamBGoals: DataTypes.INTEGER,
		gamedayID: DataTypes.INTEGER,
		start_time: DataTypes.DATE
	});

	Match.associate = function (models) {
		models.Match.belongsTo(models.Team, {
			foreignKey: 'teamAID',
			as: 'TeamA'
		});
		models.Match.belongsTo(models.Team, {
			foreignKey: 'teamBID',
			as: 'TeamB'
		});
		models.Match.belongsTo(models.Gameday, { foreignKey: 'gamedayID' });
		models.Match.hasMany(models.Bet, { foreignKey: 'matchID' });
	};
	return Match;
};