'use strict';
module.exports = (sequelize, DataTypes) => {
	const Usertipgroup = sequelize.define('Usertipgroup', {
        userID: DataTypes.INTEGER,
        tipgroupID: DataTypes.INTEGER
	});

	Usertipgroup.associate = function (model) {
		model.Usertipgroup.belongsTo(model.Tipgroup);
	};

	return Usertipgroup;
};
