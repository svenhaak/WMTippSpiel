'use strict';
module.exports = (sequelize, DataTypes) => {
  const Group = sequelize.define('Group', {
    letter: DataTypes.STRING.BINARY,
    teamAID: DataTypes.INTEGER,
    teamBID: DataTypes.INTEGER,
    teamCID: DataTypes.INTEGER,
    teamDID: DataTypes.INTEGER
  });

  Group.associate = function (models) {
	  models.Group.belongsTo(models.Team, {
		  foreignKey: 'teamAID',
		  as: 'TeamA'
	  });
	  models.Group.belongsTo(models.Team, {
		  foreignKey: 'teamBID',
		  as: 'TeamB'
	  });
	  models.Group.belongsTo(models.Team, {
		  foreignKey: 'teamCID',
		  as: 'TeamC'
	  });
	  models.Group.belongsTo(models.Team, {
		  foreignKey: 'teamDID',
		  as: 'TeamD'
		});
  };
  return Group;
};