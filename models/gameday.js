'use strict';
module.exports = (sequelize, DataTypes) => {
	const Gameday = sequelize.define('Gameday', {
		name: DataTypes.STRING,
		isPreliminary: DataTypes.BOOLEAN
	});

	Gameday.associate = function (models) {
		models.Gameday.hasMany(models.Match, { foreignKey: 'gamedayID' });
	};
	
	return Gameday;
};