'use strict';
module.exports = (sequelize, DataTypes) => {
	const Team = sequelize.define('Team', {
		name: DataTypes.STRING,
		code: DataTypes.STRING,
		preliminaryPoints: DataTypes.INTEGER,
		goalsShot: DataTypes.INTEGER,
		goalsAgainst: DataTypes.INTEGER,
		goalDiff: DataTypes.INTEGER
	});

	Team.associate = function (models) {
		models.Team.hasOne(models.Match, { foreignKey: 'teamAID' });
		models.Team.hasOne(models.Match, { foreignKey: 'teamBID' });
	}
	return Team;
};