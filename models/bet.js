'use strict';
module.exports = (sequelize, DataTypes) => {
	const Bet = sequelize.define('Bet', {
		matchID: DataTypes.INTEGER,
		userID: DataTypes.INTEGER,
		teamAGoals: DataTypes.INTEGER,
		teamBGoals: DataTypes.INTEGER,
		points: DataTypes.INTEGER
	});

	Bet.associate = function (models) {
		models.Bet.belongsTo(models.User, { foreignKey: 'userID' });
		models.Bet.belongsTo(models.Match, { foreignKey: 'matchID' });
	};

	return Bet;
};