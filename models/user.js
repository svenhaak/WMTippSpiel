'use strict';
module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define('User', {
		prename: DataTypes.STRING,
		lastname: DataTypes.STRING,
		login: DataTypes.STRING,
		password: DataTypes.STRING,
		isAdministrator: DataTypes.BOOLEAN,
		points: DataTypes.INTEGER
	});

	User.associate = function (models) {
		models.User.hasMany(models.Bet, { foreignKey: 'userID' });
		models.User.belongsToMany(models.Tipgroup, {
			through: {
				model: models.Usertipgroup,
				unique: false,
		  },
		  foreignKey: 'userID'
		});
		models.User.hasMany(models.Usertipgroup, { foreignKey: 'userID' });
	};

	return User;
};
