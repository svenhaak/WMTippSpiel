'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Usertipgroups', {
            userID: {
                allowNull: false,
                primary: true,
                type: Sequelize.INTEGER,
                references: {
					model: 'Users',
					key: 'id'
				}
            },
            tipgroupID: {
                allowNull: false,
                primary: true,
                type: Sequelize.INTEGER,
                references: {
					model: 'Tipgroups',
					key: 'id'
				}
            },
            createdAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('UserTipgroups');
    }
};
