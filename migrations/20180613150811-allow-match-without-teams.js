'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        const teamA = queryInterface.changeColumn('Matches', 'teamAID', {
            type: Sequelize.INTEGER,
            allowNull: true
          });
         const teamB = queryInterface.changeColumn('Matches', 'teamBID', {
            type: Sequelize.INTEGER,
            allowNull: true
         });
         return Promise.all([teamA, teamB]);
    },
    down: (queryInterface, Sequelize) => {
        const teamA = queryInterface.changeColumn('Matches', 'teamAID', {
            type: Sequelize.INTEGER,
            allowNull: false
          });
         const teamB = queryInterface.changeColumn('Matches', 'teamBID', {
            type: Sequelize.INTEGER,
            allowNull: false
         });
         return Promise.all([teamA, teamB]);
    }
};