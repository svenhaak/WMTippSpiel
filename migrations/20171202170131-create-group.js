'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Groups', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			letter: {
				type: Sequelize.CHAR
			},
			teamAID: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Teams',
					key: 'id'
				}
			},
			teamBID: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Teams',
					key: 'id'
				}
			},
			teamCID: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Teams',
					key: 'id'
				}
			},
			teamDID: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Teams',
					key: 'id'
				}
			},
            createdAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            }
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Groups');
	}
};