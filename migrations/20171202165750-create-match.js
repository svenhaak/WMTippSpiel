'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Matches', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			teamAID: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Teams',
					key: 'id'
				}
			},
			teamAGoals: {
				type: Sequelize.INTEGER
			},
			teamBID: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Teams',
					key: 'id'
				}
			},
			teamBGoals: {
				type: Sequelize.INTEGER
			},
			gamedayID: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Gamedays',
					key: 'id'
				}
			},
			start_time: {
				allowNull: false,
				type: Sequelize.DATE
			},
            createdAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            }
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Matches');
	}
};
