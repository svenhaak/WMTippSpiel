'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        const shot = queryInterface.addColumn('Teams', 'goalsShot', {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
         });
        const against = queryInterface.addColumn('Teams', 'goalsAgainst', {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        });
        const diff = queryInterface.addColumn('Teams', 'goalDiff', {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        });
        return Promise.all([shot, against, diff]);
    },
    down: (queryInterface, Sequelize) => {
        const shot = queryInterface.removeColumn('Teams', 'goalsShot');
        const against = queryInterface.removeColumn('Teams', 'goalsAgainst');
        const diff = queryInterface.removeColumn('Teams', 'goalDiff');
        return Promise.all([shot, against, diff]);
    }
};