'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.addConstraint('Bets', ['matchID',	'userID'],
			{
				type:'unique',
				name:'UC_unique_bet'
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.removeConstraint('Bets', 'UC_unique_bet');
		;
	}
};