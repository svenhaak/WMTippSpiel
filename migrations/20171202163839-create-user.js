'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Users', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
			prename: {
				allowNull: false,
                type: Sequelize.STRING
            },
			lastname: {
				allowNull: false,
                type: Sequelize.STRING
            },
			login: {
                allowNull: false,
                primaryKey: true,
                type: Sequelize.STRING
            },
			password: {
				allowNull: false,
                type: Sequelize.STRING
			},
			isAdministrator: {
                allowNull: false,
				type: Sequelize.BOOLEAN
			},
            points: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 0
            },
            createdAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Users');
    }
};
