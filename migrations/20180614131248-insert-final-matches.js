'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Matches', [{
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-06-30 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-06-30 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-01 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-01 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-02 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-02 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-03 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-03 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 5,
			start_time: '2018-07-06 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 5,
			start_time: '2018-07-06 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 5,
			start_time: '2018-07-07 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 5,
			start_time: '2018-07-07 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 6,
			start_time: '2018-07-10 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 6,
			start_time: '2018-07-11 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 7,
			start_time: '2018-07-15 15:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 8,
			start_time: '2018-07-14 14:00'
		}
		], {});
	},

	down: (queryInterface, Sequelize) => { //TODO
		return queryInterface.bulkDelete('Matches', null);
	}
};
