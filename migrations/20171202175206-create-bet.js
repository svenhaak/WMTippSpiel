'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Bets', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			matchID: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Matches',
					key: 'id'
				}
			},
			userID: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Users',
					key: 'id'
				}
			},
			teamAGoals: {
				allowNull: false,
				type: Sequelize.INTEGER
			},
			teamBGoals: {
				allowNull: false,
				type: Sequelize.INTEGER
			},
			points: {
				allowNull: false,
				type: Sequelize.INTEGER,
				defaultValue: 0
			},
            createdAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3)'),
                type: Sequelize.DATE
            }
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Bets');
	}
};