﻿'use strict';
const debug = require('debug');
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const flashMsg = require('cookie-flash-messages')
const session = require('express-session');

const routes = require('./routes/index');
const player = require('./routes/player');
const administration = require('./routes/administration')

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(flashMsg);
app.use(session({
	secret: (app.get('env') === 'development') ? 'spooky' : process.env.SESSION_SECRET,
	resave: false,
	saveUninitialized: true
}));

app.use('/', routes);
app.use('/', administration);
app.use('/', player);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            title: 'Es ist ein Fehler vom Typ ' + res.statusCode + ' aufgetreten',
            message: err.message,
			stack: err.stack,
			user: req.session.user
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        title: 'Es ist ein Fehler vom Typ ' + res.statusCode + ' aufgetreten',
        message: err.message,
        stack: null,
		user: req.session.user
    });
});

app.set('port', process.env.PORT || 3000);

const server = app.listen(app.get('port'), function () {
    debug('Express server listening on port ' + server.address().port);
});
