﻿'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Matches', [{
			teamAID: 2,
			teamAGoals: null,
			teamBID: 3,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-14 15:00'
		}, {
			teamAID: 1,
			teamAGoals: null,
			teamBID: 4,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-15 12:00'
		}, {
			teamAID: 6,
			teamAGoals: null,
			teamBID: 5,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-15 15:00'
		}, {
			teamAID: 7,
			teamAGoals: null,
			teamBID: 8,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-15 18:00'
		}, {
			teamAID: 11,
			teamAGoals: null,
			teamBID: 9,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-16 10:00'
		}, {
			teamAID: 13,
			teamAGoals: null,
			teamBID: 15,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-16 13:00'
		}, {
			teamAID: 12,
			teamAGoals: null,
			teamBID: 10,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-16 16:00'
		}, {
			teamAID: 14,
			teamAGoals: null,
			teamBID: 16,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-16 19:00'
		}, {
			teamAID: 18,
			teamAGoals: null,
			teamBID: 20,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-17 12:00'
		}, {
			teamAID: 21,
			teamAGoals: null,
			teamBID: 23,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-17 15:00'
		}, {
			teamAID: 17,
			teamAGoals: null,
			teamBID: 19,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-17 18:00'
		}, {
			teamAID: 24,
			teamAGoals: null,
			teamBID: 22,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-18 12:00'
		}, {
			teamAID: 25,
			teamAGoals: null,
			teamBID: 27,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-18 15:00'
		}, {
			teamAID: 28,
			teamAGoals: null,
			teamBID: 26,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-18 18:00'
		}, {
			teamAID: 29,
			teamAGoals: null,
			teamBID: 30,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-19 12:00'
		}, {
			teamAID: 31,
			teamAGoals: null,
			teamBID: 32,
			teamBGoals: null,
			gamedayID: 1,
			start_time: '2018-06-19 15:00'
		}, {
			teamAID: 2,
			teamAGoals: null,
			teamBID: 1,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-19 18:00'
		}, {
			teamAID: 7,
			teamAGoals: null,
			teamBID: 6,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-20 12:00'
		}, {
			teamAID: 4,
			teamAGoals: null,
			teamBID: 3,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-20 15:00'
		}, {
			teamAID: 5,
			teamAGoals: null,
			teamBID: 8,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-20 18:00'
		}, {
			teamAID: 10,
			teamAGoals: null,
			teamBID: 9,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-21 12:00'
		}, {
			teamAID: 11,
			teamAGoals: null,
			teamBID: 12,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-21 15:00'
		}, {
			teamAID: 13,
			teamAGoals: null,
			teamBID: 14,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-21 18:00'
		}, {
			teamAID: 17,
			teamAGoals: null,
			teamBID: 18,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-22 12:00'
		}, {
			teamAID: 16,
			teamAGoals: null,
			teamBID: 15,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-22 15:00'
		}, {
			teamAID: 20,
			teamAGoals: null,
			teamBID: 19,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-22 18:00'
		}, {
			teamAID: 25,
			teamAGoals: null,
			teamBID: 28,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-23 12:00'
		}, {
			teamAID: 22,
			teamAGoals: null,
			teamBID: 23,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-23 15:00'
		}, {
			teamAID: 21,
			teamAGoals: null,
			teamBID: 24,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-23 18:00'
		}, {
			teamAID: 26,
			teamAGoals: null,
			teamBID: 27,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-24 12:00'
		}, {
			teamAID: 30,
			teamAGoals: null,
			teamBID: 32,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-24 15:00'
		}, {
			teamAID: 31,
			teamAGoals: null,
			teamBID: 29,
			teamBGoals: null,
			gamedayID: 2,
			start_time: '2018-06-24 18:00'
		}, {
			teamAID: 4,
			teamAGoals: null,
			teamBID: 2,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-25 14:00'
		}, {
			teamAID: 3,
			teamAGoals: null,
			teamBID: 1,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-25 14:00'
		}, {
			teamAID: 8,
			teamAGoals: null,
			teamBID: 6,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-25 18:00'
		}, {
			teamAID: 5,
			teamAGoals: null,
			teamBID: 7,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-26 18:00'
		}, {
			teamAID: 10,
			teamAGoals: null,
			teamBID: 11,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-26 14:00'
		}, {
			teamAID: 9,
			teamAGoals: null,
			teamBID: 12,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-26 14:00'
		}, {
			teamAID: 15,
			teamAGoals: null,
			teamBID: 14,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-26 18:00'
		}, {
			teamAID: 16,
			teamAGoals: null,
			teamBID: 13,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-26 18:00'
		}, {
			teamAID: 22,
			teamAGoals: null,
			teamBID: 21,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-27 14:00'
		}, {
			teamAID: 23,
			teamAGoals: null,
			teamBID: 24,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-27 14:00'
		}, {
			teamAID: 19,
			teamAGoals: null,
			teamBID: 18,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-27 18:00'
		}, {
			teamAID: 20,
			teamAGoals: null,
			teamBID: 17,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-27 18:00'
		}, {
			teamAID: 32,
			teamAGoals: null,
			teamBID: 29,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-28 14:00'
		}, {
			teamAID: 30,
			teamAGoals: null,
			teamBID: 31,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-28 14:00'
		}, {
			teamAID: 26,
			teamAGoals: null,
			teamBID: 25,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-28 18:00'
		}, {
			teamAID: 27,
			teamAGoals: null,
			teamBID: 28,
			teamBGoals: null,
			gamedayID: 3,
			start_time: '2018-06-28 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-06-30 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-06-30 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-01 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-01 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-02 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-02 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-03 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 4,
			start_time: '2018-07-03 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 5,
			start_time: '2018-07-06 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 5,
			start_time: '2018-07-06 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 5,
			start_time: '2018-07-07 14:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 5,
			start_time: '2018-07-07 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 6,
			start_time: '2018-07-10 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 6,
			start_time: '2018-07-11 18:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 7,
			start_time: '2018-07-15 15:00'
		}, {
			teamAID: null,
			teamAGoals: null,
			teamBID: null,
			teamBGoals: null,
			gamedayID: 8,
			start_time: '2018-07-14 14:00'
		}
		], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Matches', null, {});
	}
};
