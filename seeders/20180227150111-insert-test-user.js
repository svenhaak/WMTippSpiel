﻿'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Users', [{
			prename: 'Test',
			lastname: 'User',
			login: 'test',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Administrator',
			lastname: 'Account',
			login: 'admin',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: true
		}]);
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Users', null, {});
	}
};
