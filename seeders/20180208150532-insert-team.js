﻿'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Teams', [{
			name: 'Ägypten',
			code: 'eg'
		}, {
			name: 'Russland',
			code: 'ru'
		}, {
			name: 'Saudi-Arabien',
			code: 'sa'
		}, {
			name: 'Uruguay',
			code: 'uy'
		}, {
			name: 'Iran',
			code: 'ir'
		}, {
			name: 'Marokko',
			code: 'ma'
		}, {
			name: 'Portugal',
			code: 'pt'
		}, {
			name: 'Spanien',
			code: 'es'
		}, {
			name: 'Australien',
			code: 'au'
		}, {
			name: 'Dänemark',
			code: 'dk'
		}, {
			name: 'Frankreich',
			code: 'fr'
		}, {
			name: 'Peru',
			code: 'pe'
		}, {
			name: 'Argentinien',
			code: 'ar'
		}, {
			name: 'Kroatien',
			code: 'hr'
		}, {
			name: 'Island',
			code: 'is'
		}, {
			name: 'Nigeria',
			code: 'ng'
		}, {
			name: 'Brasilien',
			code: 'br'
		}, {
			name: 'Costa Rica',
			code: 'cr'
		}, {
			name: 'Schweiz',
			code: 'ch'
		}, {
			name: 'Serbien',
			code: 'rs'
		}, {
			name: 'Deutschland',
			code: 'de'
		}, {
			name: 'Südkorea',
			code: 'kr'
		}, {
			name: 'Mexiko',
			code: 'mx'
		}, {
			name: 'Schweden',
			code: 'se'
		}, {
			name: 'Belgien',
			code: 'be'
		}, {
			name: 'England',
			code: 'gb-eng'
		}, {
			name: 'Panama',
			code: 'pa'
		}, {
			name: 'Tunesien',
			code: 'tn'
		}, {
			name: 'Kolumbien',
			code: 'co'
		}, {
			name: 'Japan',
			code: 'jp'
		}, {
			name: 'Polen',
			code: 'pl'
		}, {
			name: 'Senegal',
			code: 'sn'
		}], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Teams', null, {});
	}
};
