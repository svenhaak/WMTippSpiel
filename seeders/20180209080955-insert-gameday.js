﻿'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Gamedays', [{
			name: 'Gruppenphase Tag 1',
			isPreliminary: true
		}, {
			name: 'Gruppenphase Tag 2',
			isPreliminary: true
		}, {
			name: 'Gruppenphase Tag 3',
			isPreliminary: true
		}, {
			name: 'Achtelfinale',
			isPreliminary: false
		}, {
			name: 'Viertelfinale',
			isPreliminary: false
		}, {
			name: 'Halbfinale',
			isPreliminary: false
		}, {
			name: 'Finale',
			isPreliminary: false
		}, {
			name: 'Spiel um Platz 3',
			isPreliminary: false
		}], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Gamedays', null, {});
	}
};
