﻿'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Users', [{
			prename: 'Lukas',
			lastname: 'Werner',
			login: 'lukwe',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Nicola',
			lastname: 'Neher',
			login: 'Ryan Stecken',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Tobias',
			lastname: 'Zelt',
			login: 'Tomenco',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Martin',
			lastname: 'Schmitt',
			login: 'Kortesch',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Gary',
			lastname: 'Höppner',
			login: 'Mowlow',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Marcel',
			lastname: 'Antic',
			login: 'manjizz',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Nils',
			lastname: 'Hollenbach',
			login: 'Nils Holgerson',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Frederike',
			lastname: 'Fechner',
			login: 'Hexe Lilly',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Tino',
			lastname: 'Stachel',
			login: 'Onti Talesch',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Dario',
			lastname: 'Weiß',
			login: 'Dirk',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Anette',
			lastname: 'Kott',
			login: 'Annedde Kot',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Lorenz',
			lastname: 'Mindner',
			login: 'Ostblockschlampe',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Christian',
			lastname: 'Dietschi',
			login: 'RektalSchakal69',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Sven',
			lastname: 'Haak',
			login: '404gHaakWurst',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Tassilo',
			lastname: 'Breu',
			login: 'FordFocusLiebe123',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Colin',
			lastname: 'McMicken',
			login: '404ColinNotFound',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Fabio',
			lastname: 'Steinhoff',
			login: 'Steinpilz',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Mayk',
			lastname: 'Hegewald',
			login: 'Per Verser',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Benedikt',
			lastname: 'Bersch',
			login: 'Tunichtgut',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Yannick',
			lastname: 'Rein',
			login: 'Yannick',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Michael',
			lastname: 'Hörrmann',
			login: 'Michi',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Jürgen',
			lastname: 'Schultheis',
			login: 'Kraut',
			password: 'sha1$c684cecf$1$c396ae65d663ac8c404fe96357afc6e1dd1841b1',
			isAdministrator: false
		}, {
			prename: 'Matthias',
			lastname: 'Moosbrugger',
			login: 'm.moos',
			password: 'sha1$43b61bd7$1$e3cf0f90c547ea8bc889e4a763ddd4ee227ba15b',
			isAdministrator: false
		}
		
	]);
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Users', null, {});
	}
};
