﻿'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Groups', [{
			letter: 'A',
			teamAID: 1,
			teamBID: 2,
			teamCID: 3,
			teamDID: 4,
		}, {
			letter: 'B',
			teamAID: 5,
			teamBID: 6,
			teamCID: 7,
			teamDID: 8,
		}, {
			letter: 'C',
			teamAID: 9,
			teamBID: 10,
			teamCID: 11,
			teamDID: 12,
		}, {
			letter: 'D',
			teamAID: 13,
			teamBID: 14,
			teamCID: 15,
			teamDID: 16,
		}, {
			letter: 'E',
			teamAID: 17,
			teamBID: 18,
			teamCID: 19,
			teamDID: 20,
		}, {
			letter: 'F',
			teamAID: 21,
			teamBID: 22,
			teamCID: 23,
			teamDID: 24,
		}, {
			letter: 'G',
			teamAID: 25,
			teamBID: 26,
			teamCID: 27,
			teamDID: 28,
		}, {
			letter: 'H',
			teamAID: 29,
			teamBID: 30,
			teamCID: 31,
			teamDID: 32,
		}], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Groups', null, {});
	}
};
